import React, { useState } from 'react'
import { Button, Form } from 'react-bootstrap'
import './style.css'

export const LoginForm = () => {
    //make onChange to validate the value 
    //make interface to submit the form 
    const [email, setEmail] = useState<string | null | undefined>();
    const [password, setPassword] = useState("");


    function isValidEmail(email: any) {
        if (email !== null && email !== undefined) {
            // return /\S+@\S+\.\S+/.test(email);
        }
    }

    const inputEmail = () => {
        if (!isValidEmail) {

        } else {

        }

    }

    function isValidPassword() {
        let i = 0;
        let character = '';
        let isLength = false;
        let isUpper = false;
        let isLower = false;
        let isDigit = false;

        if (password.length > 8) {
            isLength = true;
        } else {
            console.log("password should be at least 8 characters long")
        }

        while (i <= password.length) {
            character = password.charAt(i);
            if (character == character.toUpperCase()) {
                isUpper = true;
            }
            if (character == character.toLowerCase()) {
                isLower = true;
            }
            if (typeof (character) == "number") {
                isDigit = true;
            }
        }
        i++;
        if (isLength && isUpper && isLower && isDigit) {
            return password
        }

    }
    const inputPassword = () => {

    }



    return (
        <Form className="container">
            <Form.Group className='mb-3 inner-container' controlId='formBasicName'>
                <div>
                    <Form.Label>First Name</Form.Label>
                    <Form.Control type="FirstName" placeholder="Tom" />
                </div>
                <div>
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control type="LastName" placeholder="Chan" />
                </div>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email" />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicGender">
                <Form.Label>Gender</Form.Label>
                <Form.Select >
                    <option>Male</option>
                    <option>Female</option>

                </Form.Select>
            </Form.Group>

            {/* change to date selection */}
            <Form.Group className="mb-3" controlId="formBasicBirthday">
                <Form.Label>Birthday</Form.Label>
                <Form.Control type="Birthday" placeholder="Birthday" />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPhonenumber">
                <Form.Label>Phone number</Form.Label>
                <Form.Control type="PhoneNumber" placeholder="Phone number" />
            </Form.Group>

            <div className='submit-reset-container'>
                <div>
                    <Button variant="dark" type="submit">
                        reset
                    </Button>

                </div>
                <div>
                    <Button variant="dark" type="submit">
                        Submit
                    </Button>
                </div>



            </div>
        </Form>
    )
}
